import React from "react";
import { shallow, mount } from "enzyme";
import Brand from "../components/atoms/Brand";
import Button from "../components/atoms/Button";
import ShowCard from "../components/atoms/ShowCard";
import Header from "../components/layouts/Header";
import Footer from "../components/layouts/Footer";
import { DATA_LOADED } from "../redux/constants";
import { Home } from "../components/pages/Home";
import renderer, { act } from "react-test-renderer";
import { Provider } from "react-redux";
import store from "../redux/store";
import App from "../App";
import { MemoryRouter } from "react-router-dom";
import Series from "../components/pages/Series";
import Movies from "../components/pages/Movies";

const feed = require("../../public/feed/sample.json");

describe("Testing Atoms", () => {
  it("Component: Brand", () => {
    const props = { to: "/", children: "Brand test" };
    const component = shallow(<Brand to={props.to}></Brand>);
    component.find("Link").simulate("click");
    expect(component).toMatchSnapshot();
  });

  it("Component: Button", () => {
    const component = shallow(
      <Button
        action={() => {
          console.log("Button component click simulation test");
        }}
      >
        a
      </Button>
    );
    component.simulate("click");
    expect(component).toMatchSnapshot();
  });

  it("Component: ShowCard", () => {
    const props = {
      title: "title",
      description: "description",
      to: "/series",
    };
    const component = shallow(<ShowCard {...props} />);
    component.find("Link").simulate("click");
    expect(component).toMatchSnapshot();
  });
});

describe("Testing Layouts", () => {
  it("Component: Header", () => {
    const component = shallow(<Header />);
    expect(component).toMatchSnapshot();
  });

  it("Component: Footer", () => {
    const component = shallow(<Footer />);
    expect(component).toMatchSnapshot();
  });
});

describe("Testing connect components", () => {
  let wrapperApp, wrapperHome, wrapperSeries, wrapperMovies;
  beforeEach(() => {
    wrapperApp = renderer.create(
      <Provider store={store}>
        <MemoryRouter>
          <App />
        </MemoryRouter>
      </Provider>
    );
    wrapperHome = renderer.create(
      <Provider store={store}>
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      </Provider>
    );
    wrapperSeries = renderer.create(
      <Provider store={store}>
        <MemoryRouter>
          <Series />
        </MemoryRouter>
      </Provider>
    );
    wrapperMovies = renderer.create(
      <Provider store={store}>
        <MemoryRouter>
          <Movies />
        </MemoryRouter>
      </Provider>
    );
  });
  it("Connected component: App", () => {
    act(() => {
      store.dispatch({ type: DATA_LOADED, payload: feed });
    });
    expect(wrapperApp).toMatchSnapshot();
  });
  it("Connected component: Home", () => {
    act(() => {
      store.dispatch({ type: DATA_LOADED, payload: feed });
    });
    expect(wrapperHome).toMatchSnapshot();
  });
  it("Connected component: Series", () => {
    act(() => {
      store.dispatch({ type: DATA_LOADED, payload: feed });
    });
    expect(wrapperSeries).toMatchSnapshot();
  });
  it("Connected component: Movies", () => {
    act(() => {
      store.dispatch({ type: DATA_LOADED, payload: feed });
    });
    expect(wrapperMovies).toMatchSnapshot();
  });

  afterAll(async (done) => {
    done();
  });
});

import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as R from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <R.BrowserRouter>
        <App />
      </R.BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

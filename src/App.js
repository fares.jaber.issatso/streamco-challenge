import React from "react";
import * as R from "react-router-dom";
import Home from "./components/pages/Home";
import Series from "./components/pages/Series";
import Movies from "./components/pages/Movies";
import NotFound from "./components/pages/NotFound";
import {
  withFooter,
  withHeader,
  withContainer,
} from "./components/layouts/wrappers";
import styled from "styled-components";
import { loadData } from "./redux/actions/data";
import { connect } from "react-redux";

function App({ loadData }) {
  React.useEffect(() => {
    loadData();
  }, [loadData]);
  return (
    <Wrapper>
      <R.Switch>
        <R.Route exact path="/" component={withHeader(withContainer(Home))} />
        <R.Route
          exact
          path="/series"
          component={withHeader(withContainer(Series))}
        />
        <R.Route
          exact
          path="/movies"
          component={withHeader(withContainer(Movies))}
        />
        <R.Route path="*" component={withHeader(withContainer(NotFound))} />
      </R.Switch>
    </Wrapper>
  );
}

export default connect(null, { loadData })(withFooter(App));

const Wrapper = styled.div`
  min-height: 100vh;
  padding-bottom: 20vh;
  @media (max-width: 768px) {
    min-height: unset;
    padding-bottom: 3rem;
  }
`;

import axios from "axios";
import { DATA_LOADED, DATA_LOAD_FAILED } from "../constants";

export const loadData = () => async (dispatch) => {
  const feed = await axios.get("/feed/sample.json");
  if (feed) {
    dispatch({
      type: DATA_LOADED,
      payload: feed.data,
    });
    return DATA_LOADED;
  } else {
    dispatch({
      type: DATA_LOAD_FAILED,
    });
    return DATA_LOAD_FAILED;
  }
};

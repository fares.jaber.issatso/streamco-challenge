import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

const initState = {};

const middleware = [thunk];

const store = createStore(reducers, initState, applyMiddleware(...middleware));

export default store;

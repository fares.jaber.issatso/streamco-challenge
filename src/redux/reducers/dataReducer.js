import { DATA_LOADED, DATA_LOAD_FAILED } from "../constants";

const initState = {
  data: null,
  loading: true,
  filter: [],
  filteredData: null,
  maxPerPage: 21,
  page: 1,
};

export default function (state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case DATA_LOADED:
      return {
        ...state,
        loading: false,
        data: payload,
      };
    case DATA_LOAD_FAILED:
      return {
        ...state,
        loading: false,
        data: null,
      };
    default:
      return state;
  }
}

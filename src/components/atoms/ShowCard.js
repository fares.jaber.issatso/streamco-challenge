import React from "react";
import PropTypes from "prop-types";
import placeholder from "../../assets/placeholder.png";
import styled from "styled-components";
import { Link } from "react-router-dom";

const ShowCard = ({
  background = placeholder,
  title,
  description,
  to = "#",
  className = "",
}) => {
  return (
    <Card className={className} background={background}>
      <Link to={to}>
        <div className="thumbnail">
          <h3>{title}</h3>
        </div>
        <div className="description">{description}</div>
      </Link>
    </Card>
  );
};

ShowCard.propTypes = {
  background: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  to: PropTypes.string,
  className: PropTypes.string,
};

export default ShowCard;

const Card = styled.article`
  position: relative;
  margin-bottom: 3rem;
  .thumbnail {
    display: table;
    width: 100%;
    height: 300px;
    background: black;
    background-image: url(${({ background }) => background});
    background-size: cover;
    background-position: center;
    h3 {
      display: table-cell;
      vertical-align: middle;
      color: white;
      text-align: center;
    }
  }
`;

import React from "react";
import { Link } from "react-router-dom";

const Brand = ({ children, link = "/" }) => {
  return <Link to={link}>{children}</Link>;
};

export default Brand;

import React from "react";
import styled from "styled-components";
import colors from "../../assets/variables.scss";

export default ({ children, action }) => {
  return <Button onClick={action}>{children}</Button>;
};

const Button = styled.button`
  padding: 5px 10px;
  color: white;
  background-color: ${colors.paragraph};
  border: 2px solid ${colors.paragraph};
  font-weight: 600;
  transition: all 0.6s ease;
  &:hover {
    background-color: ${colors.secondary};
  }
`;

import React from "react";
import styled from "styled-components";
import colors from "../../assets/variables.scss";
import { Link } from "react-router-dom";
import facebook from "../../assets/social/facebook-white.svg";
import facebookHover from "../../assets/social/facebook-blue.svg";
import instagram from "../../assets/social/instagram-white.svg";
import instagramHover from "../../assets/social/instagram-blue.svg";
import twitter from "../../assets/social/twitter-white.svg";
import twitterHover from "../../assets/social/twitter-blue.svg";
import appStore from "../../assets/store/app-store.svg";
import playStore from "../../assets/store/play-store.svg";
import windowsStore from "../../assets/store/windows-store.svg";

const Footer = () => {
  return (
    <BottomSection>
      <div className="container">
        <div className="important-links">
          <ul>
            <li>
              <Link className="inverted" to="#">
                Home
              </Link>
            </li>
            <li>
              <Link className="inverted" to="#">
                Terms and Conditions
              </Link>
            </li>
            <li>
              <Link className="inverted" to="#">
                Privacy Policy
              </Link>
            </li>
            <li>
              <Link className="inverted" to="#">
                Collection Statement
              </Link>
            </li>
            <li>
              <Link className="inverted" to="#">
                Help
              </Link>
            </li>
            <li>
              <Link className="inverted" to="#">
                Manage Account
              </Link>
            </li>
          </ul>
        </div>
        <div className="copyright">
          Copyright &copy; 2016 DEMO Streaming. All rights reserved.
        </div>
        <div className="social">
          <div className="row">
            <div className="left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <Link className="social-icon facebook" to="#"></Link>
              <Link className="social-icon instagram" to="#"></Link>
              <Link className="social-icon twitter" to="#"></Link>
            </div>
            <div className="right col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <Link className="download-link" to="#">
                <img src={appStore} alt="App store" />
              </Link>
              <Link className="download-link" to="#">
                <img src={playStore} alt="Play store" />
              </Link>
              <Link className="download-link" to="#">
                <img src={windowsStore} alt="Windows store" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </BottomSection>
  );
};

export default Footer;

const BottomSection = styled.footer`
  position: absolute;
  @media (max-width: 768px) {
    position: relative;
  }
  bottom: 0;
  background-color: ${colors.paragraph};
  width: 100%;
  padding: 2rem 0;
  .download-link {
    margin: 10px 20px;
    display: inline-block;
    img {
      height: 40px;
    }
    &:first-child {
      margin-left: 0;
      @media (max-width: 768px) {
        margin-left: 20px;
      }
    }
    &:last-child {
      margin-right: 0;
      @media (max-width: 768px) {
        margin-right: 20px;
      }
    }
  }
  .social {
    margin-top: 2rem;
    .left {
      text-align: left;
      @media (max-width: 768px) {
        text-align: center;
      }
      margin-top: 10px;
      .social-icon {
        display: inline-block;
        width: 35px;
        height: 35px;
        margin: 0 20px;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        transition: background-image 0.6s ease;
        cursor: pointer;
        &.facebook {
          background-image: url(${facebook});
          &:hover {
            background-image: url(${facebookHover});
          }
        }
        &.instagram {
          background-image: url(${instagram});
          &:hover {
            background-image: url(${instagramHover});
          }
        }
        &.twitter {
          background-image: url(${twitter});
          &:hover {
            background-image: url(${twitterHover});
          }
        }
      }
      .social-icon:last-child {
        margin-right: 0;
      }
      .social-icon:first-child {
        margin-left: 0;
      }
    }
    .right {
      text-align: ${window.innerWidth > 767 ? "right" : "center"};
      margin-top: 10px;
    }
  }
  .copyright {
    color: ${colors.invertedParagraph};
    font-size: 0.8rem;
    margin: 1rem 0;
  }
  .important-links {
    ul {
      padding: 0;
      margin: 0;
      li {
        color: white;
        display: inline-block;
        font-size: 0.9rem;
        padding: 0 10px;
        border-right: 1px solid white;
        line-height: 0.9rem;
      }
      li: last-child {
        border-right: 0px solid white;
        padding-right: 0;
      }
      li: first-child {
        padding-left: 0;
      }
    }
  }
`;

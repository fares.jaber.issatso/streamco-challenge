import React from "react";
import styled from "styled-components";
import colors from "../../assets/variables.scss";
import Brand from "../atoms/Brand";
import Button from "../atoms/Button";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <>
      <TopSection>
        <div className="top-bar container">
          <div className="row">
            <div className="branding col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <Brand>DEMO Stream</Brand>
            </div>
            <div className="join col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <div style={{ display: "table", height: "100%", width: "100%" }}>
                <div style={{ display: "table-cell", verticalAlign: "middle" }}>
                  <Link to="#">Log in</Link>
                  <Button action={() => {}}>Start your free trial</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </TopSection>
      <PageTitle>
        <div className="container">
          <h3>
            Popular{" "}
            {window.location.pathname === "/"
              ? "titles"
              : window.location.pathname.slice(1)}
          </h3>
        </div>
      </PageTitle>
    </>
  );
};

export default Header;

const PageTitle = styled.div`
  position: relative;
  background-color: ${colors.secondary};
  -webkit-box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  z-index: 2;
  padding: 10px 0;
  h3 {
    color: white;
  }
`;

const TopSection = styled.div`
  position: relative;
  background-color: ${colors.primary};
  padding: 10px 0;
  -webkit-box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
  z-index: 3;
  .join {
    text-align: right;
    a {
      color: white;
      font-weight: 600;
      font-size: 1rem;
      margin-right: 30px;
      &:hover {
        color: ${colors.secondary};
      }
    }
  }
  .branding {
    a {
      color: white;
      font-weight: 600;
      font-size: 2rem;
      &:hover {
        color: ${colors.secondary};
      }
    }
  }
`;

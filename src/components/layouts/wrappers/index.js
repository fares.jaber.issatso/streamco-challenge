import React from "react";
import Header from "../Header";
import Footer from "../Footer";

export const withFooter = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => (
    <>
      <WrappedComponent {...props} />
      <Footer />
    </>
  );

  return hocComponent;
};

export const withHeader = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => (
    <>
      <Header />
      <WrappedComponent {...props} />
    </>
  );

  return hocComponent;
};

export const withContainer = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => (
    <div className="container" style={{ paddingTop: "3rem" }}>
      <WrappedComponent {...props} />
    </div>
  );

  return hocComponent;
};

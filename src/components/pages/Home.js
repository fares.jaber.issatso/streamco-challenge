import React from "react";
import { connect } from "react-redux";
import ShowCard from "../atoms/ShowCard";
import PropTypes from "prop-types";

export const Home = ({ data }) => {
  return (
    <div>
      {data &&
        (data.loading ? (
          "Loading..."
        ) : data.data ? (
          <div className="row">
            <ShowCard
              className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3"
              title="Series"
              description="Popular series"
              to="/series"
            />
            <ShowCard
              className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3"
              title="Movies"
              description="Popular movies"
              to="/movies"
            />
          </div>
        ) : (
          "Oops, something went wrong..."
        ))}
    </div>
  );
};

Home.propTypes = {
  data: PropTypes.object,
};

export default connect((state) => ({ data: state.dataReducer }))(Home);

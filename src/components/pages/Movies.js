import React from "react";
import { connect } from "react-redux";
import ShowCard from "../atoms/ShowCard";
import PropTypes from "prop-types";

const Movies = ({ data }) => {
  return (
    <div>
      {data &&
        (data.loading ? (
          "Loading..."
        ) : data.data ? (
          <div className="row">
            {data.data.entries
              .filter(
                (entry) =>
                  entry.releaseYear > 2010 && entry.programType === "movie"
              )
              .sort((a, b) =>
                a.title > b.title ? 1 : b.title > a.title ? -1 : 0
              )
              .map((entry, index) => (
                <ShowCard
                  key={index}
                  className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3"
                  description={entry.title}
                  background={entry.images["Poster Art"].url}
                  to="#"
                />
              ))}
          </div>
        ) : (
          "Oops, something went wrong..."
        ))}
    </div>
  );
};

Movies.propTypes = {
  data: PropTypes.object,
};

export default connect((state) => ({ data: state.dataReducer }))(Movies);
